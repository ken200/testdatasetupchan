﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("DbSetupChanTest")]

namespace DbSetupChan.MSSQL
{
    /// <summary>
    /// テストデータでSQLServerを初期化する
    /// </summary>
    public class MSSQLInitializer : IDbInitializer
    {
        private string _connectionString;
        private Dictionary<string, TestDataTable> _targets = new Dictionary<string, TestDataTable>();

        public MSSQLInitializer(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void SetInitTarget(TestDataTable table)
        {
            _targets.Add(table.TableName, table);
        }

        public void SetInitTarget(IEnumerable<TestDataTable> tables)
        {
            foreach(var t in tables)
            {
                SetInitTarget(t);
            }
        }

        private IEnumerable<TableLink> createAllTableLink(SqlConnection con, SqlTransaction tran)
        {
            var link = new TableLink();

            foreach(var r in con.Query(@"
SELECT
    name
FROM
    sys.SYSOBJECTS
WHERE
    xtype = 'U' --UserTable
ORDER BY
    id"
, null, tran))
            {
                link.Concat(r.name);
            }

            return new TableLink[]{ link };
        }

        private IEnumerable<FKRefTableLink> createFKRefTableLink(SqlConnection con, SqlTransaction tran)
        {
            var fkLinks = new List<FKRefTableLink>();

            foreach(var r in con.Query(@"
SELECT
    fk.name AS fk_name,
    tblParent.name AS parent_name,
    tblReferenced.name AS ref_name
FROM
    sys.FOREIGN_KEYS fk INNER JOIN sys.TABLES tblParent on fk.parent_object_id = tblParent.object_id 
    INNER JOIN sys.TABLES tblReferenced on fk.referenced_object_id = tblReferenced.object_id
ORDER BY
    fk_name, parent_name
", null, tran))
            {
                var link = new FKRefTableLink() 
                { 
                    ParentTableName = r.parent_name, 
                    ReferencedTableName = r.ref_name 
                };

                if(fkLinks == null)
                {
                    fkLinks = new List<FKRefTableLink>();
                    fkLinks.Add(link);
                    continue;
                }

                for (var i = 0; i < fkLinks.Count;i++ )
                {
                    if (fkLinks[i].Concat(link))
                        goto NotNewLink;
                }
                //連結対象が存在しなかった場合にfkLinksに追加する。
                fkLinks.Add(link);

                NotNewLink:
                    continue;
            }

            return fkLinks;
        }

        /// <summary>
        /// 整合性チェック結果情報
        /// </summary>
        private class FKRefTableLinkValidationResultSet
        {
            /// <summary>
            /// <para>総合結果:対象テーブル名</para>
            /// <para>　</para>
            /// <para>先頭要素に最上位マスターテーブル名、末尾要素に下位テーブルを格納。</para>
            /// <para>(テーブル名の挿入位置決定ロジックの絡み。先頭要素が最上位の場合のほうがシンプルなコードになる為。)</para>
            /// <para>TableNamesプロパティで外部に公開する際に並びを逆にする。</para>
            /// </summary>
            private List<string> _extractResultTotal = new List<string>();

            /// <summary>
            /// <para>対象テーブル名の取得</para>
            /// <para>先頭要素が下位テーブル、末尾要素に最上位のマスターテーブル名がセットされている</para>
            /// </summary>
            public IEnumerable<string> TableNames
            {
                get 
                {
                    if (_extractResultTotal.Count == 0)
                    {
                        yield break;
                    }
                    else
                    {
                        for (var i = _extractResultTotal.Count() - 1; i >= 0; i--)
                            yield return _extractResultTotal[i];
                    }
                }
            }

            public FKRefTableLinkValidationResultSet()
            {
            }

            /// <summary>
            /// チェック結果の登録
            /// </summary>
            /// <param name="extractResult">
            /// <para>チェック結果テーブル名</para>
            /// <para>先頭要素が下位テーブル、末尾要素に最上位のマスターテーブル名がセットされている</para>
            /// </param>
            public void RegisterResult(IEnumerable<string> extractResult)
            {
                if (extractResult == null)
                    return;

                var lastIdx = _extractResultTotal.Count();
                foreach (var r in extractResult)
                {
                    var currentIdx = _extractResultTotal.IndexOf(r);
                    if (currentIdx < 0)
                    {
                        _extractResultTotal.Insert(lastIdx, r);
                    }
                    else
                    {
                        lastIdx = currentIdx;
                    }
                }
            }
        }

        private void initializeInternal(SqlConnection con, SqlTransaction tran)
        {
            var targetTableNames = _targets.Select((r) => r.Key);
            var fkLinks = createFKRefTableLink(con, tran);

            executeDelete(con, tran, fkLinks.GetOrderedTableNames(createAllTableLink(con, tran)));

            if(fkLinks.Count() > 0)
            {
                var validTotal = new FKRefTableLinkValidationResultSet();

                foreach(var link in fkLinks)
                {
                    IEnumerable<string> fkTableNames = null;
                    var valid = link.TryExtractionAndValidation(targetTableNames, out fkTableNames);
                    if(valid)
                        validTotal.RegisterResult(fkTableNames);
                }

                executeInsert(con, tran, validTotal.TableNames);
                executeInsert(con, tran, targetTableNames.Where((a) => !validTotal.TableNames.Any((b) => b == a)));
            }
            else
            {
                executeInsert(con, tran, targetTableNames);
            }
        }

        private void executeDeleteInsert(SqlConnection con, SqlTransaction tran, IEnumerable<string> tableNames)
        {
            executeDelete(con, tran, tableNames);
            executeInsert(con, tran, tableNames);
        }

        /// <summary>
        /// Deleteの実行
        /// </summary>
        /// <param name="con"></param>
        /// <param name="tran"></param>
        /// <param name="tableNames">
        /// <para>テーブル名</para>
        /// <para>先頭要素が下位テーブル、末尾要素に最上位のマスターテーブル名がセットされている</para>
        /// </param>
        private void executeDelete(SqlConnection con, SqlTransaction tran, IEnumerable<string> tableNames)
        {
            if (tableNames.Count() == 0)
                return;

            Console.WriteLine("executeDelete() : {0}", tableNames.Aggregate((a, b) => a + "," + b));

            foreach (var t in tableNames)
            {
                con.Execute(@"DELETE FROM " + t, null, tran);
            }
        }

        /// <summary>
        /// Insertの実行
        /// </summary>
        /// <param name="con"></param>
        /// <param name="tran"></param>
        /// <param name="tableNames">
        /// <para>テーブル名</para>
        /// <para>先頭要素が下位テーブル、末尾要素に最上位のマスターテーブル名がセットされている</para>
        /// </param>
        private void executeInsert(SqlConnection con, SqlTransaction tran, IEnumerable<string> tableNames)
        {
            if (tableNames.Count() == 0)
                return;

            Console.WriteLine("executeInsert() : {0}", tableNames.Reverse().Aggregate((a, b) => a + "," + b));

            //tableNames引数値の元ネタは_target。
            //このメソッドの中で_targets[tableNames[X]]みたいな感じで_targetsからテストデータ取り出しているのがダサい。。

            foreach (var t in tableNames.Reverse())
            {
                foreach (var row in _targets[t].Rows)
                {
                    var excludeNull = row.Where((r) => r.Value != null);

                    con.Execute("INSERT INTO " + _targets[t].TableName
                        + "("
                        + excludeNull
                            .Select((r) => r.FieldName)
                            .Aggregate((a, b) => a + "," + b)
                        + ")"
                        + "VALUES"
                        + "("
                        + excludeNull
                            .Select((r) => "@" + r.FieldName)
                            .Aggregate((a, b) => a + "," + b)
                        + ")"
                        , excludeNull.CreateParamObject()
                        , tran);
                }
            }
        }

        public void Initialize()
        {
            var con = new SqlConnection(_connectionString);
            SqlTransaction tran = null;

            try
            {
                con.Open();
                tran = con.BeginTransaction();
                initializeInternal(con, tran);
                tran.Commit();
                con.Close();
            }
            catch
            {
                if (tran != null)
                    tran.Rollback();
                con.Close();
                throw;
            }
        }
    }

    public static class TestDataFieldEnumerableExtentions
    {
        public static DynamicParameters CreateParamObject(this IEnumerable<TestDataField> fields)
        {
            var obj = new DynamicParameters();
            foreach (var f in fields)
            {
                obj.Add(f.FieldName, f.Value);
            }
            return obj;
        }
    }

    internal interface ITableLink
    {
        string[] TableNames { get; }
    }

    internal class TableLink : ITableLink
    {
        private List<string> _tableNames = new List<string>();

        public void Concat(string tname)
        {
            _tableNames.Add(tname);
        }

        public string[] TableNames
        {
            get { return _tableNames.ToArray(); }
        }
    }

    internal class FKRefTableLink : ITableLink
    {
        /// <summary>
        /// 参照しているテーブル名
        /// </summary>
        public string ParentTableName { get; set; }

        /// <summary>
        /// <para>参照されているテーブル名</para>
        /// <para>(マスターテーブル側)</para>
        /// </summary>
        public string ReferencedTableName { get; set; }

        /// <summary>
        /// <para>リンク情報</para>
        /// <para>先頭要素から下位テーブル、末尾要素のReferencedTableNameに最上位のマスターテーブル名がセットされる</para>
        /// </summary>
        private List<FKRefTableLink> _links = new List<FKRefTableLink>();

        public FKRefTableLink()
        {
            _links.Add(this);
        }

        /// <summary>
        /// <para>外部キー参照関係の連結</para>
        /// <para>インスタンスの末尾のリンクのReferencedTableNameとパラメーターのParentTableNameが一致する場合にリンクを連結する</para>
        /// <para>連結の有無を真偽値で返す</para>
        /// </summary>
        /// <param name="nextLink"></param>
        /// <returns></returns>
        public bool Concat(FKRefTableLink nextLink)
        {
            //ネストされた外部キー制約の中間の制約と一致した場合は既に連結されていると判断。
            for (var i = 0; i < _links.Count;i++ )
            {
                if(_links[i].ParentTableName == nextLink.ParentTableName
                    && _links[i].ReferencedTableName == nextLink.ReferencedTableName)
                {
                    return true;
                }
            }

            var prevLink = _links.Last();
            if (prevLink.ReferencedTableName != nextLink.ParentTableName)
                return false;

            _links.Add(nextLink);
            return true;
        }

        /// <summary>
        /// <para>関係するテーブル名称リスト</para>
        /// <para>先頭要素が下位テーブル、末尾要素に最上位のマスターテーブル名がセットされている</para>
        /// </summary>
        public string[] TableNames
        {
            get 
            {
                return _links
                    .Select((r) => r.ParentTableName)
                    .Concat(new[] { _links.Last().ReferencedTableName })
                    .ToArray();
            }
        }

        /// <summary>
        /// <para>リンクの部分一致確認</para>
        /// <para>リンク構成テーブル名リストに含まれているテーブル名を抽出し、outパラメーターにセットする。</para>
        /// <para>バリデーションチェックとして、返すテーブル名リストが部分一致としてマッチするかを確認。</para>
        /// <para>結果をメソッドの戻り値として真偽値で返す。</para>
        /// <para>　</para>
        /// <para>※部分一致: </para>
        /// <para>抽出テーブル名が最上位から連続して任意の位置まで続いている状態を指す。</para>
        /// <para>リンクが「(下位)A->B->C->D(上位)」の場合、「A->B->C->D」「B->C->D」「C->D」が部分一致となる。</para>
        /// </summary>
        /// <param name="target">
        /// <para>チェック対象テーブル名</para>
        /// <para>並びは不定</para>
        /// </param>
        /// <param name="extracted">
        /// <para>リンク構造構成に一致したテーブル名</para>
        /// <para>先頭要素が下位テーブル、末尾要素に最上位のマスターテーブル名がセットされている</para>
        /// </param>
        /// <returns></returns>
        public bool TryExtractionAndValidation(IEnumerable<string> target, out IEnumerable<string> extracted)
        {
            var matchTables = new List<string>();
            var tableInLink = this.TableNames;

            var lastMatchIdx = -1;
            var validateResult = true;
            for (var i=tableInLink.Length-1; i >= 0; i--)
            {
                var matched = target.FirstOrDefault((r) => r == tableInLink[i]);
                if (matched == null)
                {
                    //最上位位置でNGだったら整合性チェックNG
                    if (lastMatchIdx == -1)
                        validateResult = false;
                }
                else
                {
                    matchTables.Insert(0, matched);

                    //前回マッチ位置から連続していない場合は整合性チェックNG
                    if (lastMatchIdx - i > 1)
                        validateResult = false;

                    lastMatchIdx = i;
                }
            }

            extracted = matchTables.Count > 0 ? matchTables : null;
            return validateResult;
        }
    }

    internal static class ITableLinkExtentions
    {
        /// <summary>
        /// テーブル名のソート結果の取得
        /// </summary>
        /// <param name="links"></param>
        /// <returns>
        /// <para>先頭要素が下位テーブル、末尾要素に最上位のマスターテーブル名がセットされている</para>
        /// </returns>
        public static IEnumerable<string> GetOrderedTableNames(this IEnumerable<ITableLink> links)
        {
            var result = new List<string>();
            var lastIdx = 0;
            foreach(var l in links)
            {
                foreach(var t in l.TableNames)
                {
                    var currentIdx = result.IndexOf(t);
                    if(currentIdx < 0)
                    {
                        result.Insert(lastIdx, t);
                    }else
                    {
                        lastIdx = currentIdx;
                    }
                }
            }

            result.Reverse();
            return result;
        }

        public static IEnumerable<string> GetOrderedTableNames(this IEnumerable<ITableLink> firstLinks, IEnumerable<ITableLink> secondLinks)
        {
            var tnames1 = firstLinks.GetOrderedTableNames();
            var tnames2 = secondLinks.GetOrderedTableNames().Where((a) => !tnames1.Any((b) => a == b));
            return tnames1.Concat(tnames2);
        }
    }
}
