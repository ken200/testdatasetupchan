﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DbSetupChan;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace DbSetupChanTest
{
    [TestClass]
    public class TSVDataSourceReaderTest
    {
        [TestMethod]
        public void データソースの読込み()
        {
            var hogeTableTsvStream = new MemoryStream(
                Encoding.UTF8.GetBytes(@"id name age address detail memo create_at
1 ほげ1 30 住所1 詳細1 メモ1 2016-10-1
2 ほげ2 31 住所2 詳細2 メモ2 2016-10-2
3 ほげ3 32 住所3 詳細3 メモ3 2016-10-3
4 ほげ4 33 住所4 詳細4 メモ4 2016-10-4
5 ほげ5 34 住所5 詳細5 メモ5 2016-10-5".Replace(" ", "\t")));

            var reader = new TSVDataSourceReader("hoge", hogeTableTsvStream);
            var testData = reader.Read("hoge");

            Assert.AreEqual("hoge", testData.TableName);
            Assert.AreEqual(5, testData.Rows.Count());

            foreach(var r in testData.Rows.Select((a, idx) => new {fields = a, idx = idx}))
            {
                Assert.AreEqual(7, r.fields.Count());
                var fieldsAry = r.fields.ToArray();

                //フィールド名
                Assert.AreEqual("id", fieldsAry[0].FieldName);
                Assert.AreEqual("name", fieldsAry[1].FieldName);
                Assert.AreEqual("age", fieldsAry[2].FieldName);
                Assert.AreEqual("address", fieldsAry[3].FieldName);
                Assert.AreEqual("detail", fieldsAry[4].FieldName);
                Assert.AreEqual("memo", fieldsAry[5].FieldName);
                Assert.AreEqual("create_at", fieldsAry[6].FieldName);

                //id列値
                Assert.AreEqual(string.Format("{0}",r.idx+1), fieldsAry[0].Value);
                //name列値
                Assert.AreEqual(string.Format("ほげ{0}", r.idx + 1), fieldsAry[1].Value);
                //age列値
                Assert.AreEqual(string.Format("{0}", 30 + r.idx), fieldsAry[2].Value);
                //aggress列値
                Assert.AreEqual(string.Format("住所{0}", r.idx + 1), fieldsAry[3].Value);
                //detail列値
                Assert.AreEqual(string.Format("詳細{0}", r.idx + 1), fieldsAry[4].Value);
                //memo列値
                Assert.AreEqual(string.Format("メモ{0}", r.idx + 1), fieldsAry[5].Value);
                //create_at列値
                Assert.AreEqual(string.Format("2016-10-{0}", r.idx + 1), fieldsAry[6].Value);
            }
        }

        [TestMethod]
        public void フォルダからデータソースの読込み()
        {
            var reader = TSVDataSourceReader.CreateInstance(@"../../TestData/TSVテストデータ");

            var hoge = reader.Read("hoge");
            {
                Assert.IsNotNull(hoge);
                Assert.AreEqual("hoge", hoge.TableName);
                Assert.AreEqual(5, hoge.Rows.Count());

                foreach (var r in hoge.Rows.Select((a, idx) => new { fields = a, idx = idx }))
                {
                    Assert.AreEqual(7, r.fields.Count());
                    var fieldsAry = r.fields.ToArray();

                    //フィールド名
                    Assert.AreEqual("id", fieldsAry[0].FieldName);
                    Assert.AreEqual("name", fieldsAry[1].FieldName);
                    Assert.AreEqual("age", fieldsAry[2].FieldName);
                    Assert.AreEqual("address", fieldsAry[3].FieldName);
                    Assert.AreEqual("detail", fieldsAry[4].FieldName);
                    Assert.AreEqual("memo", fieldsAry[5].FieldName);
                    Assert.AreEqual("create_at", fieldsAry[6].FieldName);

                    //id列値
                    Assert.AreEqual(string.Format("{0}", r.idx + 1), fieldsAry[0].Value);
                    //name列値
                    Assert.AreEqual(string.Format("ほげ{0}", r.idx + 1), fieldsAry[1].Value);
                    //age列値
                    Assert.AreEqual(string.Format("{0}", 30 + r.idx), fieldsAry[2].Value);
                    //aggress列値
                    Assert.AreEqual(string.Format("住所{0}", r.idx + 1), fieldsAry[3].Value);
                    //detail列値
                    Assert.AreEqual(string.Format("詳細{0}", r.idx + 1), fieldsAry[4].Value);
                    //memo列値
                    Assert.AreEqual(string.Format("メモ{0}", r.idx + 1), fieldsAry[5].Value);
                    //create_at列値
                    Assert.AreEqual(string.Format("2016-10-{0}", r.idx + 1), fieldsAry[6].Value);
                }
            }

            var foo = reader.Read("foo");
            {
                Assert.IsNotNull(foo);
                Assert.AreEqual("foo", foo.TableName);
                Assert.AreEqual(5, foo.Rows.Count());

                foreach (var r in foo.Rows.Select((a, idx) => new { fields = a, idx = idx }))
                {
                    Assert.AreEqual(1, r.fields.Count());
                    var fieldsAry = r.fields.ToArray();
                    //フィールド名
                    Assert.AreEqual("name", fieldsAry[0].FieldName);
                    //name列値
                    Assert.AreEqual(string.Format("ふー{0}", r.idx + 1), fieldsAry[0].Value);
                }
            }

            var null2 = reader.Read("nullnull");
            {
                Assert.IsNotNull(null2);
                Assert.AreEqual("nullnull", null2.TableName);
                Assert.AreEqual(2, null2.Rows.Count());

                foreach (var r in null2.Rows.Select((a, idx) => new { fields = a, idx = idx }))
                {
                    Assert.AreEqual(7, r.fields.Count());
                    var fieldsAry = r.fields.ToArray();

                    //フィールド名
                    Assert.AreEqual("id", fieldsAry[0].FieldName);
                    Assert.AreEqual("name", fieldsAry[1].FieldName);
                    Assert.AreEqual("age", fieldsAry[2].FieldName);
                    Assert.AreEqual("address", fieldsAry[3].FieldName);
                    Assert.AreEqual("detail", fieldsAry[4].FieldName);
                    Assert.AreEqual("memo", fieldsAry[5].FieldName);
                    Assert.AreEqual("create_at", fieldsAry[6].FieldName);

                    //id列値
                    Assert.AreEqual(string.Format("{0}", r.idx + 1), fieldsAry[0].Value);
                    //name列値
                    Assert.AreEqual(string.Format("ほげ{0}", r.idx + 1), fieldsAry[1].Value);
                    //age列値
                    Assert.IsNull(fieldsAry[2].Value);
                    //aggress列値
                    Assert.AreEqual("NULL", fieldsAry[3].Value);
                    //detail列値
                    Assert.IsNull(fieldsAry[4].Value);
                    //memo列値
                    Assert.AreEqual("", fieldsAry[5].Value);
                    //create_at列値
                    Assert.AreEqual(string.Format("2016-10-{0}", r.idx + 1), fieldsAry[6].Value);
                }
            }
        }
    }
}
