﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using DbSetupChan.MSSQL;

namespace DbSetupChanTest
{
    [TestClass]
    public class FKRefTableLinkTest
    {
        private FKRefTableLink createLink(string parent, string referenced)
        {
            return new FKRefTableLink()
            {
                ParentTableName = parent,
                ReferencedTableName = referenced
            };
        }

        [TestMethod]
        public void TryExtractionAndValidationによる検証と抽出_全一致()
        {
            var link = createLink("A", "B");
            link.Concat(createLink("B", "C"));
            link.Concat(createLink("C", "D"));

            IEnumerable<string> extracted = null;
            Assert.AreEqual(true, link.TryExtractionAndValidation(new[] { "D", "C", "B", "A" }, out extracted));
            CollectionAssert.AreEqual(new[] { "A", "B", "C", "D" }, extracted.ToArray());
        }

        [TestMethod]
        public void TryExtractionAndValidationによる検証と抽出_部分一致()
        {
            var link = createLink("A", "B");
            link.Concat(createLink("B", "C"));
            link.Concat(createLink("C", "D"));

            IEnumerable<string> extracted = null;
            Assert.AreEqual(true, link.TryExtractionAndValidation(new[] { "D", "C" }, out extracted));
            CollectionAssert.AreEqual(new[] { "C", "D" }, extracted.ToArray());
        }

        [TestMethod]
        public void TryExtractionAndValidationによる検証と抽出_最小の部分一致()
        {
            //「最上位に位置するマスター系テーブルに対するテスト」を行う場合にあり得るパターン

            var link = createLink("A", "B");
            link.Concat(createLink("B", "C"));
            link.Concat(createLink("C", "D"));

            IEnumerable<string> extracted = null;
            Assert.AreEqual(true, link.TryExtractionAndValidation(new[] { "D" }, out extracted));
            CollectionAssert.AreEqual(new[] { "D" }, extracted.ToArray());
        }

        [TestMethod]
        public void TryExtractionAndValidationによる検証と抽出_不一致_未連続_最上位リンク未指定()
        {
            var link = createLink("A", "B");
            link.Concat(createLink("B", "C"));
            link.Concat(createLink("C", "D"));

            IEnumerable<string> extracted = null;
            Assert.AreEqual(false, link.TryExtractionAndValidation(new[] { "C", "B", "A" }, out extracted));
            CollectionAssert.AreEqual(new[] { "A", "B", "C" }, extracted.ToArray());
        }

        [TestMethod]
        public void TryExtractionAndValidationによる検証と抽出_不一致_未連続_任意位置のリンク未指定()
        {
            var link = createLink("A", "B");
            link.Concat(createLink("B", "C"));
            link.Concat(createLink("C", "D"));

            IEnumerable<string> extracted = null;
            Assert.AreEqual(false, link.TryExtractionAndValidation(new[] { "D", "A" }, out extracted));
            CollectionAssert.AreEqual(new[] { "A", "D" }, extracted.ToArray());
        }
    }
}
