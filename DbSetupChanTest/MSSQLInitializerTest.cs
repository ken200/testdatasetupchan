﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DbSetupChan;
using DbSetupChan.MSSQL;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;

namespace DbSetupChanTest
{
    [TestClass]
    public class MSSQLInitializerTest
    {
        private MSSQLInitializer CreateInitializer()
        {
            return new MSSQLInitializer(ConfigurationManager.ConnectionStrings["dbSetupChanTestDb"].ConnectionString);
        }

        [TestMethod]
        public void テーブル初期化確認()
        {
            var dataXmlStr = new StringBuilder(@"<Root><Table name=""TestItemMaster"">");
            for(var i=1;i<=100;i++)
            {
                dataXmlStr.AppendFormat(@"<Row>
                                    <Field name=""name""><![CDATA[商品{0}]]></Field>
                                    <Field name=""price""><![CDATA[{0}000]]></Field>
                                    <Field name=""detail"">null</Field>
                                    <Field name=""update_by""><![CDATA[{1}]]></Field>
                                    <Field name=""delete_flg""><![CDATA[{2}]]></Field>
                                </Row>", 
                                       i, 
                                       DateTime.Parse("2016/01/01 01:01:01").AddDays(i)
                                       , i % 5);
            }
            dataXmlStr.Append("</Table></Root>");

            using (var s = new MemoryStream(Encoding.UTF8.GetBytes(dataXmlStr.ToString())))
            {
                var r = new XmlDataSourceReader(s);
                var table = r.Read("TestItemMaster");
                var initializer = CreateInitializer();
                initializer.SetInitTarget(table);
                initializer.Initialize();
            }

            //エラーなく実行できればOKとする。(DBへの反映は手動確認。。)
            Assert.AreEqual(true, true);
        }

        private async Task initalizeAndTestFromXmlFile(string xmlFileName)
        {
            using (var sr = new StreamReader(new FileStream(@"..\..\TestData\" + xmlFileName, FileMode.Open)))
            {
                using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(await sr.ReadToEndAsync())))
                {
                    var reader = new XmlDataSourceReader(ms);
                    var tables = reader.ReadAll();
                    var initializer = CreateInitializer();
                    foreach (var table in tables)
                    {
                        initializer.SetInitTarget(table);
                    }
                    initializer.Initialize();
                }
            }

            //エラーなく実行できればOKとする。(DBへの反映は手動確認。。)
            Assert.AreEqual(true, true);
        }

        [TestMethod]
        public async Task テーブル初期化確認_マスターテーブル()
        {
            await initalizeAndTestFromXmlFile("マスターテーブルのみ.xml");
        }

        [TestMethod]
        public async Task テーブル初期化確認_外部キー制約テーブル_複数制約あり()
        {
            await initalizeAndTestFromXmlFile("外部キー制約テーブル_複数制約あり.xml");
        }

        [TestMethod]
        public async Task テーブル初期化確認_外部キー制約テーブル_非制約テーブルとの混在()
        {
            await initalizeAndTestFromXmlFile("非制約テーブル_外部キー制約テーブル混在.xml");
        }

        [TestMethod]
        public async Task テーブル初期化確認_外部キー制約テーブル_制約有テーブルデータ不足()
        {
            //テストデータ不正の為、クエリ発行中にFKey制約エラーでException吐く動作で問題無しとする。

            try
            {
                await initalizeAndTestFromXmlFile("非制約テーブル_外部キー制約テーブル混在_外部キー不整合あり.xml");
                Assert.Fail();
            }
            catch
            {
                return;
            }
        }

        [TestMethod]
        public async Task テーブル初期化確認_初期化対象が外部キー制約関係ないテーブルのみ()
        {
            await initalizeAndTestFromXmlFile("非制約テーブルのみ.xml");
        }

        [TestMethod]
        public void テーブル初期化確認_初期化対象未指定()
        {
            var initializer = CreateInitializer();
            initializer.Initialize();
        }
    }
}
