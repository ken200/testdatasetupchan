﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using DbSetupChan;
using DbSetupChan.MSSQL;
using Dapper;

namespace DbSetupChanTest
{
    [TestClass]
    public class TestDataFieldEnumerableExtentionsTest
    {
        [TestMethod]
        public void CreateParamObjectの生成()
        {
            var fields = new[]
            {
                new TestDataField(){FieldName = "列1", Value = 100},
                new TestDataField(){FieldName = "列2", Value = "れつ２"},
                new TestDataField(){FieldName = "列3", Value = null},
                new TestDataField(){FieldName = "列4", Value = true},
                new TestDataField(){FieldName = "列5", Value = DateTime.Parse("2016/09/20 11:22:33")}
            };

            SqlMapper.IParameterLookup obj = fields.Where((r) => r.Value != null).CreateParamObject();
            
            Assert.AreEqual(100, obj["列1"]);
            Assert.AreEqual("れつ２", obj["列2"]);

            try
            {
                Assert.AreEqual(null, obj["列3"]);
                Assert.Fail();
            }
            catch { }

            Assert.AreEqual(true, obj["列4"]);
            Assert.AreEqual(DateTime.Parse("2016/09/20 11:22:33"), obj["列5"]);
        }
    }
}
