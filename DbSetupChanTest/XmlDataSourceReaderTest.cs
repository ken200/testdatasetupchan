﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DbSetupChan;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace DbSetupChanTest
{
    [TestClass]
    public class XmlDataSourceReaderTest
    {
        private string _xmlString = @"<Root>
            <!-- コメント -->
            <Table name=""テーブル名1"">
                <!-- コメント -->
                <Row>
                    <!-- コメント -->
                    <Field name=""フィールド1""><![CDATA[値1]]></Field>
                    <!-- コメント -->
                    <Field name=""フィールド2""><![CDATA[値2]]></Field>
                    <!-- コメント -->
                    <Field name=""フィールド3""><![CDATA[値3]]></Field>
                    <!-- コメント -->
                    <Field name=""フィールド4""><![CDATA[値4]]></Field>
                </Row>
                <!-- 
                複数行コメント 
                複数行コメント 
                -->
                <Row>
                    <!-- コメント -->
                    <!-- コメント -->
                    <Field name=""フィールド1""><![CDATA[値11]]></Field>
                    <!-- コメント -->
                    <!-- コメント -->
                    <Field name=""フィールド2"">NULL</Field>
                    <!-- コメント -->
                    <!-- コメント -->
                    <Field name=""フィールド3""><![CDATA[
複数行の文字列１
    複数行の文字列２
        複数行の文字列３
    複数行の文字列４
複数行の文字列５
]]></Field>
                    <!-- コメント -->
                    <!-- コメント -->
                    <Field name=""フィールド4""><![CDATA[値44]]></Field>
                </Row>
            </Table>
            <!-- コメント -->
            <Table name=""テーブル名2"">
                <!-- コメント -->
                <Row>
                    <!-- コメント -->
                    <Field name=""フィールドA""><![CDATA[値A]]></Field>
                    <!-- コメント -->
                    <Field name=""フィールドB""><![CDATA[値B]]></Field>
                    <!-- コメント -->
                    <Field name=""フィールドC""><![CDATA[値C]]></Field>
                </Row>
                <!-- コメント -->
                <Row>
                    <!-- コメント -->
                    <Field name=""フィールドA""><![CDATA[値AA]]></Field>
                    <!-- コメント -->
                    <Field name=""フィールドB"">NULL</Field>
                    <!-- コメント -->
                    <Field name=""フィールドC""><![CDATA[値CC]]></Field>
                </Row>
            </Table>
        </Root>";


        [TestMethod]
        public void データソースの読込み()
        {
            using(var s = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(_xmlString)))
            {
                var reader = new XmlDataSourceReader(s);

                {
                    var table1 = reader.Read("テーブル名1");
                    Assert.AreEqual("テーブル名1", table1.TableName);

                    var table1Rows = table1.Rows.ToArray();
                    Assert.AreEqual(2, table1Rows.Length);

                    var table1Row1Fields = table1Rows[0].ToArray();
                    Assert.AreEqual(4, table1Row1Fields.Length);
                    Assert.AreEqual("フィールド1", table1Row1Fields[0].FieldName);
                    Assert.AreEqual("フィールド2", table1Row1Fields[1].FieldName);
                    Assert.AreEqual("フィールド3", table1Row1Fields[2].FieldName);
                    Assert.AreEqual("フィールド4", table1Row1Fields[3].FieldName);
                    Assert.AreEqual("値1", table1Row1Fields[0].Value);
                    Assert.AreEqual("値2", table1Row1Fields[1].Value);
                    Assert.AreEqual("値3", table1Row1Fields[2].Value);
                    Assert.AreEqual("値4", table1Row1Fields[3].Value);

                    var table1Row2Fields = table1Rows[1].ToArray();
                    Assert.AreEqual(4, table1Row2Fields.Length);
                    Assert.AreEqual("フィールド1", table1Row2Fields[0].FieldName);
                    Assert.AreEqual("フィールド2", table1Row2Fields[1].FieldName);
                    Assert.AreEqual("フィールド3", table1Row2Fields[2].FieldName);
                    Assert.AreEqual("フィールド4", table1Row2Fields[3].FieldName);
                    Assert.AreEqual("値11", table1Row2Fields[0].Value);
                    Assert.AreEqual(null, table1Row2Fields[1].Value);
                    var multiRowExpectedVal = @"
複数行の文字列１
    複数行の文字列２
        複数行の文字列３
    複数行の文字列４
複数行の文字列５
".Replace("\r\n", "\n"); //なんでか改行コードが\nだけ。。
                    Assert.AreEqual(multiRowExpectedVal, table1Row2Fields[2].Value);
                    Assert.AreEqual("値44", table1Row2Fields[3].Value);

                }

                {
                    var table2 = reader.Read("テーブル名2");
                    Assert.AreEqual("テーブル名2", table2.TableName);

                    var table2Rows = table2.Rows.ToArray();
                    Assert.AreEqual(2, table2Rows.Length);

                    var table2Row1Fields = table2Rows[0].ToArray();
                    Assert.AreEqual(3, table2Row1Fields.Length);
                    Assert.AreEqual("フィールドA", table2Row1Fields[0].FieldName);
                    Assert.AreEqual("フィールドB", table2Row1Fields[1].FieldName);
                    Assert.AreEqual("フィールドC", table2Row1Fields[2].FieldName);
                    Assert.AreEqual("値A", table2Row1Fields[0].Value);
                    Assert.AreEqual("値B", table2Row1Fields[1].Value);
                    Assert.AreEqual("値C", table2Row1Fields[2].Value);

                    var table2Row2Fields = table2Rows[1].ToArray();
                    Assert.AreEqual(3, table2Row2Fields.Length);
                    Assert.AreEqual("フィールドA", table2Row2Fields[0].FieldName);
                    Assert.AreEqual("フィールドB", table2Row2Fields[1].FieldName);
                    Assert.AreEqual("フィールドC", table2Row2Fields[2].FieldName);
                    Assert.AreEqual("値AA", table2Row2Fields[0].Value);
                    Assert.AreEqual(null, table2Row2Fields[1].Value);
                    Assert.AreEqual("値CC", table2Row2Fields[2].Value);
                }
            }
        }

        [TestMethod]
        public void データソースの読込み_デフォルト値あり()
        {
            var xml = @"<Root>
                <!-- コメント -->
                <Table name=""テーブル名1"">
                    <!-- デフォルト値 -->
                    <DefaultValue>
                        <!-- フィールド1のデフォルト値 -->
                        <Field name=""フィールド1""><![CDATA[デフォルト1]]></Field>
                        <!-- フィールド2のデフォルト値 -->
                        <Field name=""フィールド2""><![CDATA[デフォルト2]]></Field>
                        <!-- フィールド3のデフォルト値 -->
                        <Field name=""フィールド3""><![CDATA[デフォルト3]]></Field>
                    </DefaultValue>
                    <!-- 行1 -->
                    <Row>
                        <!-- フィールド1 -->
                        <Field name=""フィールド1""><![CDATA[行1のフィールド1の値]]></Field>
                        <!-- フィールド2 -->
                        <Field name=""フィールド2""><![CDATA[行1のフィールド2の値]]></Field>
                    </Row>
                    <!-- 行2 -->
                    <Row>
                        <Field name=""フィールド1""><![CDATA[行2のフィールド1の値]]></Field>
                        <Field name=""フィールド2""><![CDATA[行2のフィールド2の値]]></Field>
                        <Field name=""フィールド3""><![CDATA[行2のフィールド3の値]]></Field>
                    </Row>
                    <!-- 行3 -->
                    <Row><!-- 全てデフォルト値を使う --></Row>
                </Table>
            </Root>";

            using (var s = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml)))
            {
                var reader = new XmlDataSourceReader(s);
                {
                    var table1 = reader.Read("テーブル名1");
                    Assert.AreEqual("テーブル名1", table1.TableName);

                    var table1Rows = table1.Rows.ToArray();
                    Assert.AreEqual(3, table1Rows.Length);

                    var table1Row1Fields = table1Rows[0].ToArray();
                    {
                        Assert.AreEqual(3, table1Row1Fields.Length);
                        Assert.AreEqual("フィールド1", table1Row1Fields[0].FieldName);
                        Assert.AreEqual("フィールド2", table1Row1Fields[1].FieldName);
                        Assert.AreEqual("フィールド3", table1Row1Fields[2].FieldName);
                        Assert.AreEqual("行1のフィールド1の値", table1Row1Fields[0].Value);
                        Assert.AreEqual("行1のフィールド2の値", table1Row1Fields[1].Value);
                        Assert.AreEqual("デフォルト3", table1Row1Fields[2].Value);
                    }

                    var table1Row2Fields = table1Rows[1].ToArray();
                    {
                        Assert.AreEqual(3, table1Row2Fields.Length);
                        Assert.AreEqual("フィールド1", table1Row2Fields[0].FieldName);
                        Assert.AreEqual("フィールド2", table1Row2Fields[1].FieldName);
                        Assert.AreEqual("フィールド3", table1Row2Fields[2].FieldName);
                        Assert.AreEqual("行2のフィールド1の値", table1Row2Fields[0].Value);
                        Assert.AreEqual("行2のフィールド2の値", table1Row2Fields[1].Value);
                        Assert.AreEqual("行2のフィールド3の値", table1Row2Fields[2].Value);
                    }

                    var table1Row3Fields = table1Rows[2].ToArray();
                    {
                        Assert.AreEqual(3, table1Row3Fields.Length);
                        Assert.AreEqual("フィールド1", table1Row3Fields[0].FieldName);
                        Assert.AreEqual("フィールド2", table1Row3Fields[1].FieldName);
                        Assert.AreEqual("フィールド3", table1Row3Fields[2].FieldName);
                        Assert.AreEqual("デフォルト1", table1Row3Fields[0].Value);
                        Assert.AreEqual("デフォルト2", table1Row3Fields[1].Value);
                        Assert.AreEqual("デフォルト3", table1Row3Fields[2].Value);
                    }
                }
            }
        }
    }
}
