﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Linq;

namespace DbSetupChan
{
    public class XmlDataSourceReader : IDataSourceReader
    {
        //<Root>
        //    <Table name="＜テーブル名1＞">
        //         <!--全レコードで同じ値をセットするフィールドの値を指定する。
        //        Rowノードでの明示的な指定がある場合はそちらが優先される-->
        //        <DefaultValue>
        //            <Field name="フィールド1"><![CDATA[デフォルト値1]]></Field>
        //            <Field name="フィールド2"><![CDATA[デフォルト値2]]></Field>
        //        </DefaultValue>
        //        <Row>
        //            <Field name="フィールド2"><![CDATA[値2]]></Field>
        //            <Field name="フィールド3"><![CDATA[値3]]></Field>
        //            <Field name="フィールド4"><![CDATA[値4]]></Field>
        //        </Row>
        //        <Row>
        //            <Field name="フィールド1"><![CDATA[値11]]></Field>
        //            <!-- NULLセットされたフィールドはInsertしない -->
        //            <Field name="フィールド2">NULL</Field>
        //            <Field name="フィールド3"><![CDATA[値33]]></Field>
        //            <Field name="フィールド4"><![CDATA[値44]]></Field>
        //        </Row>
        //    </Table>
        //    <Table name="＜テーブル名2＞">
        //        <Row>
        //            <Field name="フィールド1"><![CDATA[値A]]></Field>
        //            <Field name="フィールド2"><![CDATA[値B]]></Field>
        //            <Field name="フィールド3"><![CDATA[値C]]></Field>
        //        </Row>
        //        <Row>
        //            <Field name="フィールド1"><![CDATA[値AA]]></Field>
        //            <Field name="フィールド2">NULL</Field>
        //            <Field name="フィールド3"><![CDATA[値CC]]></Field>
        //        </Row>
        //    </Table>
        //</Root>


        private XDocument _xdoc;

        public XmlDataSourceReader(Stream xmlStream)
        {
            _xdoc = XDocument.Load(xmlStream);
        }

        private IEnumerable<XElement> MergeFieldValue(XElement defaultEle, XElement rowEle)
        {
            var rowFieldEles = rowEle.Elements("Field");
            if (defaultEle == null)
                return rowFieldEles;
            var defaultEleList = defaultEle.Elements("Field").ToList();

            foreach(var r in rowFieldEles)
            {
                var idx = defaultEleList
                    .FindIndex((a) => a.Attribute("name").Value == r.Attribute("name").Value);

                if(idx == -1)
                {
                    defaultEleList.Add(r);
                }
                else
                {
                    defaultEleList[idx] = r;
                }
            }

            return defaultEleList;
        }

        public TestDataTable Read(string tableName)
        {
            var table = _xdoc.Root.Elements("Table")
                .First((x) => x.Attribute("name").Value == tableName);
            var defaultEle = table.Element("DefaultValue");
            var rowEles = table.Elements("Row");

            return new TestDataTable()
            {
                TableName = tableName,
                Rows = rowEles.Select((x) =>
                {
                    var xx = MergeFieldValue(defaultEle, x);
                    return new TestDataRow(xx.Select((xxx) =>
                        new TestDataField()
                        {
                            FieldName = xxx.Attribute("name").Value,
                            Value = xxx.FirstNode.NodeType == System.Xml.XmlNodeType.CDATA ? xxx.Value : null
                        }));
                })
            };
        }

        public IEnumerable<TestDataTable> ReadAll()
        {
            return _xdoc.Root.Elements("Table").Select((x) => Read(x.Attribute("name").Value));
        }
    }
}
