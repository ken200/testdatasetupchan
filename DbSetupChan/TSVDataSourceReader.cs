﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DbSetupChan
{
    public class TSVDataSourceReader : IDataSourceReader
    {
        private class TSVData
        {
            public string TableName {get;set;}
            
            public IEnumerable<string> FieldNames {get; private set;}

            public IEnumerable<IEnumerable<string>> RecordDatas {get; private set;}

            public TSVData(string tableName, string fieldNameTsvRecord)
            {
                TableName = tableName;
                FieldNames = fieldNameTsvRecord.Split('\t');
                RecordDatas = new List<IEnumerable<string>>();
            }

            public void AddRecord(string recordValue)
            {
                var splited = recordValue.Split('\t');
                ((List<IEnumerable<string>>)RecordDatas)
                    .Add(splited.Select((a) =>
                    {
                        if (a.ToUpper() == "NULL")
                            return null;
                        else if (a.ToUpper() == "\"NULL\"")
                            return "NULL";
                        else
                            return a;
                    }));
            }
        }

        private Dictionary<string, TSVData> _tsvs = new Dictionary<string, TSVData>();

        /// <summary>
        /// Readerオブジェクトの生成
        /// </summary>
        /// <param name="tsvFilesDirPath">TSVファイルが格納されているフォルダパス</param>
        /// <returns></returns>
        public static TSVDataSourceReader CreateInstance(string tsvFilesDirPath)
        {
            var tsvDelayedStreams = new List<Tuple<string, Func<Stream>>>();

            foreach(var f in Directory.GetFiles(tsvFilesDirPath, "*.tsv", SearchOption.TopDirectoryOnly))
            {
                tsvDelayedStreams.Add(new Tuple<string, Func<Stream>>(
                    Path.GetFileNameWithoutExtension(f),
                    () => new FileStream(f,FileMode.Open,FileAccess.Read,FileShare.Read)));
            }

            return new TSVDataSourceReader(tsvDelayedStreams.Select((a) =>
            {
                //using (var s = a.Item2())
                //{
                //    return new Tuple<string, Stream>(a.Item1, s);
                //}
                return new Tuple<string, Stream>(a.Item1, a.Item2());
            }));
        }

        public TSVDataSourceReader(IEnumerable<Tuple<string, Stream>> tsvStreams)
        {
            foreach(var s in tsvStreams)
            {
                using(var fs = new StreamReader(s.Item2))
                {
                    var tsv = AppendTsvData(s.Item1, fs.ReadLine());
                    while (!fs.EndOfStream) 
                    {
                        tsv.AddRecord(fs.ReadLine());
                    }
                }
            }
        }

        public TSVDataSourceReader(string tableName, Stream tsvStream)
        {
            using (var fs = new StreamReader(tsvStream))
            {
                var tsv = AppendTsvData(tableName, fs.ReadLine());
                while (!fs.EndOfStream)
                {
                    tsv.AddRecord(fs.ReadLine());
                }
            }
        }

        private TSVData AppendTsvData(string tableName, string fieldNameTsvRecord)
        {
            if (!_tsvs.ContainsKey(tableName))
                _tsvs.Add(tableName, new TSVData(tableName, fieldNameTsvRecord));
            return _tsvs[tableName];
        }

        public TestDataTable Read(string tableName)
        {
            if (!_tsvs.ContainsKey(tableName))
                return null;
            var tsv = _tsvs[tableName];
            return new TestDataTable()
            {
                TableName = tsv.TableName,
                Rows = tsv.RecordDatas.Select((a) => 
                {
                    return new TestDataRow(a.Zip(tsv.FieldNames, (b,c) => new {fname = c, val = b}).Select((d) =>
                    {
                        return new TestDataField()
                        {
                            FieldName = d.fname,
                            Value = d.val
                        };
                    }));
                })
            };
        }

        public IEnumerable<TestDataTable> ReadAll()
        {
            return _tsvs.Keys.Select((a) => Read(a));
        }
    }
}
