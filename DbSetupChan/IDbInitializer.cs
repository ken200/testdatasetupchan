﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbSetupChan
{
    public class TestDataField
    {
        public string FieldName { get; set; }
        public object Value { get; set; }
    }

    public class TestDataRow : IEnumerable<TestDataField>
    {
        private IEnumerable<TestDataField> _fields = null;

        public IEnumerator<TestDataField> GetEnumerator()
        {
            return _fields.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public TestDataRow(IEnumerable<TestDataField> fields)
        {
            _fields = fields;
        }
    }

    public class TestDataTable
    {
        public string TableName { get; set; }
        public IEnumerable<TestDataRow> Rows { get; set; }
    }

    public interface IDataSourceReader
    {
        TestDataTable Read(string tableName);
        IEnumerable<TestDataTable> ReadAll();
    }

    /// <summary>
    /// テストデータでデータベースを初期化する
    /// </summary>
    public interface IDbInitializer
    {
        /// <summary>
        /// <para>挿入するテストデータの登録</para>
        /// <para>登録後はInitialize()でテストデータのInsertが行われること。</para>
        /// </summary>
        /// <param name="table"></param>
        void SetInitTarget(TestDataTable table);
        /// <summary>
        /// <para>挿入するテストデータの登録</para>
        /// <para>登録後はInitialize()でテストデータのInsertが行われること。</para>
        /// </summary>
        /// <param name="table"></param>
        void SetInitTarget(IEnumerable<TestDataTable> tables);
        /// <summary>
        /// <para>初期化</para>
        /// <para>SetInitTarget()で登録したテストデータをInsertする。処理前に全テーブル内容がクリアされる。</para>
        /// <para>Insert対象が存在しない場合は、挿入処理は何も行わない。テーブルクリアだけ行う。</para>
        /// </summary>
        void Initialize();
    }
}
